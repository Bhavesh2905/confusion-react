import React, { Component } from 'react';
import Menu from './MenuComponent';
import DishDetail from './DishdetailComponent';
import  Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import {Switch,Route,Redirect,withRouter} from 'react-router-dom';
import Contact from './ContactComponent';
import About from './AboutComponent';
import {connect} from 'react-redux';
import {addComment,fetchDishes,fetchComments,fetchPromos} from '../redux/ActionCreators';

import {actions} from 'react-redux-form';


const mapStateToProps=state=>{
    return {
      dishes:state.dishes,
      comments:state.comments,
      promotions:state.promotions,
      leaders:state.leaders
    }
}

const mapDispatchToProps=dispatch=>({
  addComment:(dishId,rating,author,comment)=>dispatch(addComment(dishId,rating,author,comment)),
  fetchDishes:()=>{dispatch(fetchDishes())},
  
  resetFeedbackForm:()=>{dispatch(actions.reset('feedback'))},
  fetchComments:()=>{dispatch(fetchComments())},
  fetchPromos:()=>{dispatch(fetchPromos())},
})


class Main extends Component {

  
  componentDidMount(){
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
  }
  
  render() {
    const Homepage=()=>{
      return(
        <Home dish={this.props.dishes.dishes.filter((dish)=>dish.featured)[0]}
        dishesLoading={this.props.dishes.isLoading}
        dishesErrMess={this.props.dishes.errMess}
        promotion={this.props.promotions.promotions.filter((promo)=>promo.featured)[0]}
        promosLoading={this.props.promotions.isLoading}
        promosErrMess={this.props.promotions.errMess}
        
        leader={this.props.leaders.filter((leader)=>leader.featured)[0]}
        />
        //featured[0]->return the true value
      );
    }
    const DishWithId=({match})=>{
      return (
        <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishid,10))[0]} 
        isLoading={this.props.dishes.isLoading}
        ErrMess={this.props.dishes.errMess}
        
        comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishid,10))} 
        commentsErrMess={this.props.comments.errMess}
  
        addComment={this.props.addComment}
        />
      );
    }
    return (
      <div>
        <Header/>
        <Switch>
          <Route path="/home" component={Homepage} />
          <Route path="/aboutus" component={()=><About leaders={this.props.leaders}/>}/>
          <Route exact path="/menu" component={()=><Menu dishes={this.props.dishes}/>}/>
          <Route path="/menu/:dishid" component={DishWithId}/>
          <Route exact path="/contactus" component={()=><Contact resetFeedbackForm={this.props.resetFeedbackForm}/>}/>
          <Redirect to="/home"/>
        </Switch>
        <Footer/>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));